# THIS FILE IS GENERATED AUTOMATICALLY FROM THE D-I PO MASTER FILES
# The master files can be found under packages/po/
#
# DO NOT MODIFY THIS FILE DIRECTLY: SUCH CHANGES WILL BE LOST
#
# translation of d-i.po to Gujarati
#
# Debian Installer master translation file template
# Don't forget to properly fill-in the header of PO files#
# Debian Installer translators, please read the D-I i18n documentation
# in doc/i18n/i18n.txt
# Contributor:
# Kartik Mistry <kartik.mistry@gmail.com>, 2006-2013
#
#
# Translations from iso-codes:
#   - translations from ICU-3.0
#
#   Alastair McKinstry <mckinstry@debian.org>, 2004.
#   Kartik Mistry <kartik.mistry@gmail.com>, 2006, 2007, 2008.
#   Ankit Patel <ankit644@yahoo.com>, 2009,2010.
#   Sweta Kothari <swkothar@redhat.com>, 2009, 2010.
msgid ""
msgstr ""
"Project-Id-Version: d-i\n"
"Report-Msgid-Bugs-To: partman-zfs@packages.debian.org\n"
"POT-Creation-Date: 2013-05-22 22:05+0000\n"
"PO-Revision-Date: 2008-08-07 11:42+0530\n"
"Last-Translator: Kartik Mistry <kartik.mistry@gmail.com>\n"
"Language-Team: Gujarati <team@utkarsh.org>\n"
"Language: gu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl4:
#. File system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl4:
#. Short file system name (untranslatable in many languages)
#. Type: text
#. Description
#. :sl4:
#. keep it short (ideally a 3-letter acronym)
#: ../partman-zfs.templates:1001 ../partman-zfs.templates:2001
#: ../partman-zfs.templates:20001
msgid "zfs"
msgstr "zfs"

#. Type: text
#. Description
#. :sl4:
#. File system name
#: ../partman-zfs.templates:3001
msgid "ZFS file system"
msgstr "ZFS ફાઈલ સિસ્ટમ"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:4001
msgid "Separate /boot and /lib/modules mandatory for this ZFS configuration"
msgstr "ZFS રૂપરેખાંકન માટે અલગ /boot અને /lib/modules જરુરી છે"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:4001
msgid ""
"Your root file system is on a ZFS pool that uses more than one physical "
"volume."
msgstr "તમારી રુટ ફાઈલ સિસ્ટમ એ ZFS પુલ પર છે જે એક કરતાં વધુ ભૌતિક કદ વાપરે છે."

#. Type: error
#. Description
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:4001 ../partman-zfs.templates:5001
msgid ""
"The boot loader only supports this configuration for pools in Mirror or "
"Striped modes, but not RAID-Z mode."
msgstr ""
"બૂટ લોડર આ રુપરેખાંકન પુલ્સ માટે માત્ર મિરર અથવા સ્ટ્રિપ્ડ સ્થિતિમાં આધાર આપે છે, RAID-Z "
"સ્થિતિ માટે નહી."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:4001
msgid ""
"Make sure /boot and /lib/modules are on a partition using a supported ZFS "
"pool configuration, or a different file system such as UFS."
msgstr ""
"ખાતરી કરો કે /boot અને /lib/modules એ ZFS પુલ રુપરેખાંકન ઉપયોગ કરતાં પાર્ટિશન, અથવા "
"બીજી ફાઈલ સિસ્ટમ જેવી કે UFS પર હોય"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:5001
msgid "Unsupported multiple volume ZFS for ${MNT}"
msgstr "${MNT} માટે આધાર ન અપાયેલ અનેક કદ ZFS"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:5001
msgid ""
"Your ${MNT} partition is on a ZFS pool that uses more than one physical "
"volume."
msgstr "તમારું ${MNT} પાર્ટિશન એ ZFS પુલ પર છે જે એક કરતાં વધુ ભૌતિક કદ ઉપયોગ કરે છે."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:5001
msgid ""
"Make sure ${MNT} is on a partition using a supported ZFS pool configuration, "
"or a different file system such as UFS."
msgstr ""
"ખાતરી કરો કે ${MNT} એ આધારિત ZFS પુલ રુપરેખાંકન, અથવા બીજી ફાઈલ સિસ્ટમ જેવી કે UFS "
"પર હોય."

#. Type: boolean
#. Description
#. :sl4:
#. Type: boolean
#. Description
#. :sl4:
#: ../partman-zfs.templates:6001 ../partman-zfs.templates:7001
msgid "Go back to the menu and correct this problem?"
msgstr "મેનુમાં પાછા જશો અને આ સમસ્યા દૂર કરશો?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-zfs.templates:6001
msgid ""
"You have configured one or more partitions with the ZFS file system. "
"Although ZFS is supported on 32-bit i386, using it without special tuning "
"may lead to performance or stability problems due to limitations of this "
"architecture."
msgstr ""
"તમે એક અથવા વધુ પાર્ટિશનને ZFS ફાઈલ સિસ્ટમ માટે રુપરેખાંકિત કરેલ છે. ZFS એ ૩૨-બીટ i386 "
"ને આધાર આપવા છતાં, ચોક્કસ ગોઠવણી કર્યા વગરે તેને ઉપયોગ કરવા જતાં આ આર્કિટેકચરની "
"મર્યાદાઓને કારણે કાર્યક્ષમતા અને સ્થિરતાની મુશ્કેલીઓ તરફ દોરી જશે."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-zfs.templates:6001
msgid ""
"You should either use the 64-bit (amd64) version of this installer (if your "
"hardware supports this), or go back to the partitioning menu and configure "
"the partitions to use another file system."
msgstr ""
"તમારે આ સ્થાપનની ૬૪-બીટ (amd64) આવૃત્તિ (જો તમારું હાર્ડવેર આધાર આપે તો) ઉપયોગ કરવી "
"જોઈએ, અથવા પાર્ટિશન મેનુ પર પાછાં જઈને બીજી ફાઈલ સિસ્ટમ વાપરવી જોઈએ."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-zfs.templates:7001
msgid ""
"You have configured one or more partitions with the ZFS file system. Using "
"ZFS on a computer with less than 512 MB of memory may lead to stability "
"problems and is not recommended."
msgstr ""
"તમે એક અથવા વધુ પાર્ટિશનને ZFS ફાઈલ સિસ્ટમ માટે રુપરેખાંકિત કરેલ છે. ૫૧૨ એમબી કરતાં ઓછી "
"મેમરી ધરાવતાં કોમ્પ્યુટપ પર ZFS વાપરવી એ સ્થિરતા મુશ્કેલીઓ તરફ દોરી જશે અને સલાહભર્યું નથી."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-zfs.templates:7001
msgid ""
"You should go back to the partitioning menu and configure the partitions to "
"use another file system."
msgstr ""
"તમારે પાર્ટિશન કરવાનાં મેનુમાં પાછા જવું જોઈએ અને બીજી ફાઈલ સિસ્ટમ માટે પાર્ટિશન "
"રુપરેખાંકિત કરવા જોઈએ."

#. Type: text
#. Description
#. :sl4:
#: ../partman-zfs.templates:8001
msgid "Configure ZFS"
msgstr "ZFS રૂપરેખાંકિત કરો"

#. Type: text
#. Description
#. :sl4:
#. What is "in use" is a partition
#: ../partman-zfs.templates:9001
msgid "In use by ZFS pool ${VG}"
msgstr "ZFS પૂલ વડે વપરાશમાં ${VG}"

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinitive form
#: ../partman-zfs.templates:10001
msgid "Display configuration details"
msgstr "રૂપરેખાંકન વિગતો દર્શાવો"

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinitive form
#: ../partman-zfs.templates:11001
msgid "Create ZFS pool"
msgstr "ZFS પૂલ બનાવો"

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinitive form
#: ../partman-zfs.templates:12001
msgid "Delete ZFS pool"
msgstr "ZFS પૂલ દૂર કરો"

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinitive form
#: ../partman-zfs.templates:13001
msgid "Create logical volume"
msgstr "તાર્કિક સમૂહ બનાવો"

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinitive form
#: ../partman-zfs.templates:14001
msgid "Create root file system"
msgstr "રૂટ ફાઈલ સિસ્ટમ બનાવો"

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinitive form
#: ../partman-zfs.templates:15001
msgid "Delete logical volume"
msgstr "તાર્કિક સમૂહ દૂર કરો"

#. Type: text
#. Description
#. :sl4:
#. Menu entry
#. Use infinitive form
#: ../partman-zfs.templates:16001
msgid "Finish"
msgstr "સંપૂર્ણ"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-zfs.templates:17001
msgid "Write the changes to disk and configure ZFS?"
msgstr "ડિસ્કમાં ફેરફારો લખશો અને ZFS રૂપરેખાંકિત કરશો?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-zfs.templates:17001
msgid ""
"Before ZFS can be configured, the current partitioning scheme has to be "
"written to disk. These changes cannot be undone."
msgstr ""
"ZFS રુપરેખાંકિત કરી શકાય તે પહેલાં, હાલની પાર્ટિશન પધ્ધતિ ડિસ્કમાં લખાવી જ જોઇએ. આ "
"ફેરફારો પાછા ફેરવી શકાશે નહી."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-zfs.templates:17001
msgid ""
"After ZFS is configured, no additional changes to the partitioning scheme of "
"disks containing physical volumes are allowed during the installation. "
"Please decide if you are satisfied with the current partitioning scheme "
"before continuing."
msgstr ""
"ZFS વ્યવસ્થાપક રુપરેખાંકિત થઇ ગયા પછી, સ્થાપન દરમિયાન ભૌતિક કદો ધરાવતી ડિસ્કસની "
"પાર્ટિશન પધ્ધતિમાં કોઇ વધારાનાં ફેરફારો માન્ય રાખવામાં આવશે નહી. આગળ વધતાં પહેલાં "
"મહેરબાની કરી નક્કી કરો કે તમે હાલની પાર્ટિશન પધ્ધતિ સાથે સંતોષીત છો."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:18001
msgid "ZFS configuration failure"
msgstr "ZFS રૂપરેખાંકન નિષ્ફળતા"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:18001
msgid "An error occurred while writing the changes to the disks."
msgstr "ડિસ્કમાં ફેરફારો સંગ્રહ કરતી વખતે ક્ષતિ ઉદભવી."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:18001
msgid "ZFS configuration has been aborted."
msgstr "ZFS રૂપરેખાંકન પડતું મુકાયેલ છે."

#. Type: text
#. Description
#. :sl4:
#: ../partman-zfs.templates:19001
msgid "physical volume for ZFS"
msgstr "ZFS માટે ભૌતિક કદ"

#. Type: select
#. Description
#. :sl4:
#: ../partman-zfs.templates:21001
msgid "ZFS configuration action:"
msgstr "ZFS રૂપરેખાંકન ક્રિયા:"

#. Type: select
#. Description
#. :sl4:
#: ../partman-zfs.templates:21001
msgid "Summary of current ZFS configuration:"
msgstr "હાલનાં ZFS રૂપરેખાંકનનો સાર:"

#. Type: select
#. Description
#. :sl4:
#: ../partman-zfs.templates:21001
msgid ""
" Free physical volumes:  ${FREE_PVS}\n"
" Used physical volumes:  ${USED_PVS}\n"
" ZFS pools:              ${VGS}\n"
" ZFS logical volumes:    ${LVS}\n"
" ${BOOTFS}"
msgstr ""
" ખાલી ભૌતિક કદો:    ${FREE_PVS}\n"
" વપરાયેલ ભૌતિક કદો:  ${USED_PVS}\n"
" ZFS પૂલ્સ:         ${VGS}\n"
" ZFS તાર્કિક કદો:     ${LVS}\n"
" ${BOOTFS}"

#. Type: note
#. Description
#. :sl4:
#: ../partman-zfs.templates:22001
msgid "Current ZFS configuration:"
msgstr "હાલનું ZFS રૂપરેખાંકન:"

#. Type: multiselect
#. Description
#. :sl4:
#: ../partman-zfs.templates:23001
msgid "Devices for the new ZFS pool:"
msgstr "નવા ZFS પૂલ માટે ઉપકરણો:"

#. Type: multiselect
#. Description
#. :sl4:
#: ../partman-zfs.templates:23001
msgid "Please select devices for the new ZFS pool."
msgstr "મહેરબાની કરી નવા ZFS પૂલ માટે ઉપકરણો પસંદ કરો."

#. Type: multiselect
#. Description
#. :sl4:
#: ../partman-zfs.templates:23001
msgid "You can select one or more devices."
msgstr "તમે એક અથવા વધુ ઉપકરણો પસંદ કરી શકો છો."

#. Type: select
#. Description
#. :sl4:
#: ../partman-zfs.templates:24001
msgid "Multidisk mode for this ZFS pool:"
msgstr "ZFS પૂલ માટે મલ્ટિડિસ્ક સ્થિતિ:"

#. Type: select
#. Description
#. :sl4:
#: ../partman-zfs.templates:24001
msgid ""
"Please choose the mode for multidisk operations for this ZFS pool:\n"
" * Striped: similar to RAID 0 (default) - data is spread across the\n"
"            physical volumes;\n"
" * Mirror:  similar to RAID 1 - data is replicated to each physical\n"
"            volume;\n"
" * RAID-Z:  similar to RAID 5 or RAID 6 - some physical volumes\n"
"            store parity bits and data is spread across others."
msgstr ""
"આ ZFS પૂલ માટે મલ્ટિડિસ્ક ક્રિયાઓની સ્થિતિ પસંદ કરો:\n"
" * Striped: RAID 0 (મૂળભૂત) ને સમાન - માહિતી ભૌતિક કદોમાં પથરાયેલી\n"
"            હશે;\n"
" * Mirror:  RAID 1 ને સમાન - માહિતી દરેક ભૌતિક કદોમાં નકલ કરેલ\n"
"            હશે;\n"
" * RAID-Z:  RAID 5 અથવા RAID 6 ને સમાન - કેટલાંક ભૌતિક કદો પેરિટી બીટ્સ\n"
"            સંગ્રહ કરશે અને માહિતી બીજામાં પથરાયેલી હશે."

#. Type: select
#. Description
#. :sl4:
#: ../partman-zfs.templates:25001
msgid "Parity level for RAID-Z:"
msgstr "RAID-Z માટે પેરિટી સ્તર:"

#. Type: select
#. Description
#. :sl4:
#: ../partman-zfs.templates:25001
msgid ""
"Please choose the number of physical volumes that will be used to store "
"parity bits."
msgstr ""
"મહેરબાની કરી પેરિટી બિટ્સ સંગ્રહ કરવા માટે વપરાશમાં લેવામાં આવતાં ભૌતિક કદોની સંખ્યા પસંદ "
"કરો."

#. Type: string
#. Description
#. :sl4:
#: ../partman-zfs.templates:26001
msgid "ZFS pool name:"
msgstr "ZFS પૂલ નામ:"

#. Type: string
#. Description
#. :sl4:
#: ../partman-zfs.templates:26001
msgid "Please enter the name you would like to use for the new ZFS pool."
msgstr "નવાં ZFS પૂલ માટે જે નામ તમે ઉપયોગ કરવા માંગતા હોવ તેનું નામ દાખલ કરો."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:27001
msgid "No physical volumes selected"
msgstr "કોઇ ભૌતિક કદો પસંદ કરેલ નથી"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:27001
msgid ""
"No physical volumes were selected. The creation of a new ZFS pool has been "
"aborted."
msgstr "કોઇ ભૌતિક કદો પસંદ કરેલ નહોતાં. નવાં ZFS પૂલ બનાવવાનું બંધ કરવામાં આવેલ છે."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:28001
msgid "No ZFS pool name"
msgstr "કોઈ ZFS પૂલ નામ નહી"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:28001
msgid "No name for the ZFS pool has been entered. Please enter a name."
msgstr "ZFS પૂલ માટે કોઇ નામ દાખલ કરવામાં આવેલ નથી. મહેરબાની કરી નામ દાખલ કરો."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:29001
msgid "ZFS pool name already in use"
msgstr "ZFS પૂલ નામ પહેલેથી વપરાશમાં છે"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:29001
msgid ""
"The selected ZFS pool name is already in use. Please choose a different name."
msgstr "પસંદ કરેલ ZFS પૂલ નામ પહેલેથી વપરાશમાં છે. મહેરબાની કરી બીજું નામ પસંદ કરો."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:30001
msgid "Error while creating ZFS pool"
msgstr "ZFS પૂલ બનાવતી વખતે ક્ષતિ"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:30001
msgid "The ZFS pool ${VG} could not be created."
msgstr "ZFS પૂલ ${VG} બનાવી શકાતું નથી."

#. Type: error
#. Description
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:30001 ../partman-zfs.templates:41001
#: ../partman-zfs.templates:45001
msgid "Check /var/log/syslog or see virtual console 4 for the details."
msgstr "/var/log/syslog તપાસો અથવા માહિતી માટે વર્ચયુઅલ કોન્સોલ ૪ જુઓ."

#. Type: select
#. Description
#. :sl4:
#: ../partman-zfs.templates:31001
msgid "ZFS pool to delete:"
msgstr "દૂર કરવા માટેનાં ZFS પૂલ:"

#. Type: select
#. Description
#. :sl4:
#: ../partman-zfs.templates:31001
msgid "Please select the ZFS pool you wish to delete."
msgstr "મહેરબાની કરી તમે જે ZFS પુલ દૂર કરવા માંગતા હોવ તે પસંદ કરો."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:32001
msgid "No ZFS pool"
msgstr "કોઈ ZFS પૂલ નહી"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:32001
msgid "No ZFS pool has been found."
msgstr "કોઇ ZFS પુલ સમૂહો મળ્યાં નથી."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:32001
msgid "The ZFS pool may have already been deleted."
msgstr "ZFS પૂલ કદાચ પહેલેથી દૂર કરેલ છે."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-zfs.templates:33001
msgid "Really delete the ZFS pool?"
msgstr "ZFS પૂલ ખરેખર દૂર કરશો?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-zfs.templates:33001
msgid "Please confirm the removal of the ZFS pool ${VG}."
msgstr "ZFS પૂલ ${VG} દૂર કરવાની ખાતરી કરો."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:34001
msgid "Error while deleting ZFS pool"
msgstr "ZFS પૂલ દૂર કરતી વખતે ક્ષતિ આવી"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:34001
msgid ""
"The selected ZFS pool could not be deleted. One or more logical volumes may "
"currently be in use."
msgstr ""
"પસંદ કરેલ ZFS પૂલ દૂર કરી શકાતા નથી. એક અથવા વધુ તાર્કિક કદો કદાચ હાલમાં ઉપયોગમાં છે."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:35001
msgid "No ZFS pool found"
msgstr "કોઇ ZFS પૂલ મળ્યા નહી"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:35001
msgid ""
"No free ZFS pools were found for creating a new logical volume. Please "
"create another ZFS pool, or free space in an existing ZFS pool."
msgstr ""
"નવું તાર્કિક કદ બનાવવા માટે કોઇ ખાલી ZFS પૂલ મળ્યાં નહોતા. મહેરબાની કરી બીજાં ZFS પૂલ "
"બનાવો, અથવા હાજર રહેલાં ZFS પૂલમાં જગ્યા ખાલી કરો."

#. Type: string
#. Description
#. :sl4:
#: ../partman-zfs.templates:36001
msgid "Logical volume name:"
msgstr "તાર્કિક કદ નામ:"

#. Type: string
#. Description
#. :sl4:
#: ../partman-zfs.templates:36001
msgid "Please enter the name you would like to use for the new logical volume."
msgstr "નવાં તાર્કિક કદ માટે જે નામ તમે ઉપયોગ કરવા માંગતા હોવ તેનું નામ દાખલ કરો."

#. Type: select
#. Description
#. :sl4:
#: ../partman-zfs.templates:37001
msgid "ZFS pool:"
msgstr "ZFS પૂલ:"

#. Type: select
#. Description
#. :sl4:
#: ../partman-zfs.templates:37001
msgid ""
"Please select the ZFS pool where the new logical volume should be created."
msgstr "મહેરબાની કરી ZFS પૂલ પસંદ કરો જ્યાં નવું તાર્કિક કદ બનાવવું જોઇએ."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:38001
msgid "No logical volume name entered"
msgstr "કોઇ તાર્કિક કદ નામ દાખલ થયેલ નથી"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:38001
msgid "No name for the logical volume has been entered. Please enter a name."
msgstr "તાર્કિક કદ માટે કોઇ નામ દાખલ કરાયેલ નથી. મહેરબાની કરી નામ દાખલ કરો."

#. Type: error
#. Description
#. :sl4:
#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:39001 ../partman-zfs.templates:41001
msgid "Error while creating a new logical volume"
msgstr "નવું તાર્કિક કદ બનાવતી વખતે ક્ષતિ"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:39001
msgid ""
"The name ${LV} is already in use by another logical volume on the same ZFS "
"pool (${VG})."
msgstr "નામ ${LV} પહેલાથી બીજા તાર્કિક કદ દ્વારા સરખાં ZFS પૂલ (${VG}) પર વપરાયેલ છે."

#. Type: string
#. Description
#. :sl4:
#: ../partman-zfs.templates:40001
msgid "Logical volume size:"
msgstr "તાર્કિક કદ માપ:"

#. Type: string
#. Description
#. :sl4:
#: ../partman-zfs.templates:40001
msgid ""
"Please enter the size of the new logical volume. The size may be entered in "
"the following formats: 10K (Kilobytes), 10M (Megabytes), 10G (Gigabytes), "
"10T (Terabytes). The default unit is Megabytes."
msgstr ""
"મહેરબાની કરી નવા તાર્કિક કદનું માપ દાખલ કરો. માપ નીચેનાં બંધારણોમાં દાખલ કરી શકાશે: "
"૧૦K (કિલોબાઇટ્સ), ૧૦M (મેગાબાઇટ્સ), ૧૦G (Gigaબાઇટ્સ), ૧૦T (Teraબાઇટ્સ). મૂળભુત માપ "
"મેગાબાઇટ્સ છે."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:41001
msgid ""
"Unable to create a new logical volume (${LV}) on ${VG} with the new size "
"${SIZE}."
msgstr "${VG} પર નવા માપ ${SIZE} સાથે નવું તાર્કિક કદ (${LV}) બનાવવામાં અસક્ષમ."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:42001
msgid "No logical volume found"
msgstr "કોઇ તાર્કિક કદ મળ્યાં નહી"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:42001
msgid "No logical volume has been found. Please create a logical volume first."
msgstr "કોઇ તાર્કિક કદ મળ્યું નહી. મહેરબાની કરી પ્રથમ તાર્કિક કદ બનાવો."

#. Type: select
#. Description
#. :sl4:
#: ../partman-zfs.templates:43001
msgid "Logical volume:"
msgstr "તાર્કિક કદ:"

#. Type: select
#. Description
#. :sl4:
#: ../partman-zfs.templates:43001
msgid "Please select the logical volume to delete."
msgstr "દૂર કરવા માટે તાર્કિક સમૂહ પસંદ કરો."

#. Type: text
#. Description
#. :sl4:
#: ../partman-zfs.templates:44001
msgid "in VG ${VG}"
msgstr "તાક ${VG} માં"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:45001
msgid "Error while deleting the logical volume"
msgstr "તાર્કિક કદ દૂર કરતી વખતે ક્ષતિ આવી"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:45001
msgid "The logical volume ${LV} on ${VG} could not be deleted."
msgstr "${VG} પર તાર્કિક કદ ${LV} દૂર કરી શકાતું નથી."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:46001
msgid "No usable physical volumes found"
msgstr "ઉપયોગ કરી શકાય તેવા ભૌતિક કદો મળ્યાં નહી"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:46001
msgid ""
"No physical volumes (i.e. partitions) were found in this system. All "
"physical volumes may already be in use. You may also need to load some "
"required kernel modules or re-partition the hard drives."
msgstr ""
"કોઇ ભૌતિક કદો (એટલેકે પાર્ટિશનો) આ સિસ્ટમમાં મળ્યા નહી. બધા ભૌતિક કદો કદાચ ઉપયોગમાં "
"હશે. તમારે કદાચ જરુરી કર્નલ મોડ્યુલો પણ લાવવા પડશે અથવા હાર્ડ ડ્રાઇવોનું ફરી પાર્ટિશન "
"કરવું પડશે."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:47001
msgid "ZFS not available"
msgstr "ZFS પ્રાપ્ત નથી"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:47001
msgid ""
"The current kernel doesn't support ZFS. You may need to load the zfs module."
msgstr "હાલનું કર્નલ ZFSને આધાર આપતું નથી. તમારે કદાચ zfs મોડ્યુલ લાવવું પડશે."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:48001
msgid "Invalid logical volume, ZFS pool name or ZFS file system name"
msgstr "અયોગ્ય તાર્કિક કદ, ZFS પૂલ નામ અથવા ZFS ફાઇલ સિસ્ટમ નામ"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:48001
msgid ""
"Logical volumes, ZFS pool names and ZFS file systems may only contain "
"alphanumeric characters, hyphen, colon, period, and underscore. They must be "
"255 characters or less and must begin with an alphanumeric character. The "
"names \"mirror\", \"raidz\", \"spare\", and \"log\" are not allowed."
msgstr ""
"તાર્કિક કદ, ZFS પૂલ નામો અને ZFS ફાઇલ સિસ્ટમ ફક્ત આંકડાઓ અને અંગ્રેજી અક્ષરો, આડી લીટી, "
"પૂર્ણવિરામ અને અન્ડરસ્કોર જ ધરાવી શકે છે. તે ૨૫૫ કે તેથી ઓછા અક્ષરોનાં જ હોવા જોઇએ અને "
"અંગ્રેજી અક્ષરો અથવા આંકડાથી શરુ થવા જોઇએ. નામો જેવાં કે \"mirror\", \"raidz\", "
"\"spare\", and \"log\" માન્ય નથી."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:48001
msgid "Please choose a different name."
msgstr "મહેરબાની કરી બીજું નામ પસંદ કરો."

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-zfs.templates:49001
msgid "Remove existing logical volume data?"
msgstr "હાલનાં તાર્કિક કદોની માહિતી દૂર કરશો?"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-zfs.templates:49001
msgid ""
"The selected device already contains the following ZFS logical volumes and "
"ZFS pools which are about to be removed:"
msgstr ""
"પસંદ કરેલ ઉપકરણ પહેલેથી નીચેના ZFS તાર્કિક કદો અને ZFS પૂલ્સ ધરાવે છે જે દૂર કરવામાં આવી "
"રહ્યા છે:"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-zfs.templates:49001
msgid "Logical volume(s): ${LVTARGETS}"
msgstr "તાર્કિક કદો: ${LVTARGETS}"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-zfs.templates:49001
msgid "ZFS pool(s): ${VGTARGETS}"
msgstr "ZFS પૂલ્સ: ${VGTARGETS}"

#. Type: boolean
#. Description
#. :sl4:
#: ../partman-zfs.templates:49001
msgid ""
"Note that this will also permanently erase any data currently on the logical "
"volumes."
msgstr ""
"ધ્યાનમાં રાખો કે આ તાર્કિક કદોમાં રહેલી હાલની કોઇપણ માહિતી હંમેશને માટે દૂર કરી નાખશે."

#. Type: note
#. Description
#. :sl4:
#: ../partman-zfs.templates:50001
msgid "ZFS"
msgstr "ZFS"

#. Type: note
#. Description
#. :sl4:
#: ../partman-zfs.templates:50001
msgid ""
"A common situation for system administrators is to find that some disk "
"partition (usually the most important one) is short on space, while some "
"other partition is underused. ZFS can help with this."
msgstr ""
"સિસ્ટમ સંચાલકો માટે ખૂબ જ સામાન્ય પરિસ્થિતિ એ હોય છે કે કેટલાંક ડિસ્ક પાર્ટિશનમાં "
"(મોટાભાગે જે ખૂબ મહત્વનાં હોય) જગ્યાનો અભાવ હોય છે, જ્યારે કેટલાંક પાર્ટિશન બિનઉપયોગી "
"હોય છે. ZFS આમાં મદદરુપ થઈ શકે છે."

#. Type: note
#. Description
#. :sl4:
#: ../partman-zfs.templates:50001
msgid ""
"ZFS allows combining disk or partition devices (\"physical volumes\") to "
"form a virtual disk (\"ZFS pool\"), which can then be divided into virtual "
"partitions (\"logical volumes\"). ZFS pools and logical volumes may span "
"across several physical disks. New physical volumes may be added to a ZFS "
"pool at any time, and logical volumes have no size limit other than the "
"total size of the ZFS pool."
msgstr ""
"ZFS ડિસ્ક અથવા પાર્ટિશન ઉપકરણો (\"ભૌતિક કદો\") થી વર્ચ્યુઅલ ડિસ્ક (\"ZFS પૂલ્સ\") નું "
"જોડાણ કરવા દે છે, જે વર્ચ્યુઅલ પાર્ટિશનો (\"તાર્કિક કદો\") માં વિભાજીત કરી શકાય છે. "
"ZFS પૂલ્સ અને તાર્કિક કદો ઘણી ભૌતિક ડિસ્ક્સ સુધી વિસ્તૃત થઈ શકે છે. નવાં ભૌતિક કદો ZFS "
"પૂલ્સમાં ગમે તે સમયે ઉમેરી શકાય છે, અને તાર્કિક કદોને ZFS પૂલના કુલ માપ સિવાય કોઈ માપ "
"મર્યાદા નથી."

#. Type: note
#. Description
#. :sl4:
#: ../partman-zfs.templates:50001
msgid ""
"The items on the ZFS configuration menu can be used to edit ZFS pools and "
"logical volumes. After you return to the main partition manager screen, "
"logical volumes will be displayed in the same way as ordinary partitions, "
"and should be treated as such."
msgstr ""
"ZFS રુપરેખાંકન મેનુ ZFS પૂલ્સ અને તાર્કિક કદોમાં ફેરફાર કરવા માટે ઉપયોગ કરી શકાય છે. તમે "
"પાર્ટિશન સંચાલનની મુખ્ય સ્ક્રિન પર આવો પછી, તાર્કિક કદો સામાન્ય પાર્ટિશનોની જેમ જ દેખાય "
"છે અને એ જ રીતે વર્તવું જોઈએ."

#. Type: select
#. Description
#. :sl4:
#: ../partman-zfs.templates:51001
msgid "Pool to modify:"
msgstr "બદલવા માટેનું પૂલ:"

#. Type: string
#. Description
#. :sl4:
#: ../partman-zfs.templates:52001
msgid "ZFS boot file system name:"
msgstr "ZFS બૂટ ફાઇલ સિસ્ટમ નામ:"

#. Type: string
#. Description
#. :sl4:
#: ../partman-zfs.templates:52001
msgid "Please enter the name of the boot file system among the following:"
msgstr "મહેરબાની કરી નીચેનાંની સાથે બૂટ ફાઇલ સિસ્ટમનું નામ દાખલ કરો:"

#. Type: string
#. Description
#. :sl4:
#: ../partman-zfs.templates:52001
msgid ""
"This will be prefixed by ${VG}/ROOT and mounted as the root file system."
msgstr "આ ${VG}/ROOT વડે પૂવર્ગ થશે અને રૂટ ફાઇલ સિસ્ટમ વડે માઉન્ટ કરાશે."

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:53001
msgid "No file system name entered"
msgstr "કોઇ ફાઇલ સિસ્ટમ નામ દાખલ કરેલ નથી"

#. Type: error
#. Description
#. :sl4:
#: ../partman-zfs.templates:53001
msgid "No name has been entered for the root file system. Please enter a name."
msgstr "રૂટ ફાઇલ સિસ્ટમ માટે કોઇ નામ દાખલ કરેલ નથી. મહેરબાની કરી નામ દાખલ કરો."

#. Type: string
#. Description
#. :sl4:
#: ../partman-zfs.templates:54001
msgid "ZFS boot"
msgstr "ZFS બૂટ"

#. Type: text
#. Description
#. :sl4:
#: ../partman-zfs.templates:55001
msgid "Loading ZFS module..."
msgstr "ZFS મોડ્યુલ લાવે છે..."
